# SPDX-FileCopyrightText: © 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <tim.jammer@hpc-hessen.de>, <marcel.giar@hpc-hessen.de>
# 
# SPDX-License-Identifier: CC0-1.0

.SUFFIXES:

REUSE_CMD := conda run --name reuse reuse

lint:
	$(REUSE_CMD) lint  

download:
	$(REUSE_CMD) download --all 
