Copyright (c) 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>

This work is licensed under multiple licenses:

* Source code and Jupyter Notebooks are licensed under [MIT](LICENSES/MIT.txt).
* Other files are licensed under [CC0-1.0](LICENSES/CC0-1.0.txt).

Please refer to the individual files for more accurate information.

Please note: We provide the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
