# Data Analysis With Pandas 

## About this course

This repository contains material for the course ["Data analysis in Python with Pandas"](https://www.hkhlr.de/en/events/data-analysis-python-pandas-2023-06-20) offered by the Competence Center for High Performance Computing in Hessen ([HKHLR](https://www.hkhlr.de/)). It aims at teaching participants the basics of analysing data using the popular Python Pandas package. 

## Getting the course material 

The course material can be obtained by cloning this repository. E.g., from within a terminal emulator of your choice execute:

```shell
git clone https://git.rwth-aachen.de/hkhlr/data-analysis-with-pandas.git
```

Please note that you need to have Git installed. This can e.g. be done via `sudo apt-get install git` on Debian-based Linux distros or `brew install git` on macOS with [Homebrew](https://brew.sh/) installed. Git for Windows can be obtained [here](https://git-scm.com/downloads).


## Setting up the environment

Please refer to the description provided in [one of our earlier courses](https://git.rwth-aachen.de/hkhlr/scientific-data-processing-with-python) for how to setup a *Anaconda / Miniconda Python* environment. The required packages can be installed with the [`environment.yml`](./environment.yml) file that can be found at the project's root folder.

## Course Content

The course material is structured as follows:

* [Lecture material](./Lecture/Pandas-1Day/): Contains Jupyter notebooks used to teach basic usage of Pandas (mostly [`DataFrame`s](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html)).
* [Exercise material](./Exercises/Pandas_GroupAssignments/): Contains Jupyter notebooks with tasks related to a particular dataset (please refer to the individual notebooks for details).

## Licensing

Please refer to the [`LICENSE.md`](LICENSE.md) file for details of the license under which each file is distributed.